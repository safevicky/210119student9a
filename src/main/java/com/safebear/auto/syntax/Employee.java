package com.safebear.auto.syntax;


public class Employee {
    private boolean employed;
    int salary;

    public void fire(){
        employed = false;
    }

    public void employ(){
        employed = true;
    }

    public void givePayRise(){
        salary = salary + 10;
    }

    public boolean getEmployed(){
        return employed;
    }

    public void setEmployed(boolean emplyStatus){
        employed= emplyStatus;
    }

    public void setSalary(int salary){
        this.salary=salary;
    }
}
