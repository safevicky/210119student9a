package com.safebear.auto.tests;

import com.safebear.auto.utils.Properties;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

public class StepDefs {

    WebDriver driver;

    @Before
    public void setUp(){
        driver = Properties.getDriver();
    }

    @After
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Given("^I nav to the login page$")
    public void i_nav_to_the_login_page() throws Throwable {
driver.get(Properties.getUrl());
    }

    @When("^I enter login details for (.+)$")
    public void i_enter_login_details_for_a_user(String userType) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I will see the message: (.+)$")
    public void i_will_see_the_validation_message(String validationMessage) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    }
